<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_example\Command;

/**
 * An example request.
 */
class ExampleRequest {

  /**
   * ExampleRequest constructor.
   *
   * @param string $title
   *   The title.
   */
  public function __construct(
    private readonly string $title,
  ) {
  }

  /**
   * Returns the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->title;
  }

}
