<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_example\Command;

use Psr\Log\LoggerInterface;

/**
 * An example request handler.
 */
class ExampleRequestHandler {

  /**
   * ExampleRequestHandler constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(private readonly LoggerInterface $logger) {
  }

  /**
   * Process the request.
   *
   * @param \Drupal\symfony_messenger_example\Command\ExampleRequest $message
   *   The request message.
   *
   * @return \Drupal\symfony_messenger_example\Command\ExampleResponse
   *   The response.
   */
  public function __invoke(ExampleRequest $message): ExampleResponse {
    $this->logger->notice('Message with title %title received', [
      '%title' => $message->getTitle(),
    ]);

    return new ExampleResponse('Example response');
  }

}
