<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_example\Command;

use Drupal\symfony_messenger\HandlerResponseInterface;

/**
 * An example response.
 */
class ExampleResponse implements HandlerResponseInterface {

  /**
   * ExampleResponse constructor.
   *
   * @param string $response
   *   The response.
   */
  public function __construct(
    private readonly string $response,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getResponse(): string {
    return $this->response;
  }

}
