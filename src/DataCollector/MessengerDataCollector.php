<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger\DataCollector;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\webprofiler\DataCollector\HasPanelInterface;
use Drupal\webprofiler\DumpTrait;
use Symfony\Component\Messenger\DataCollector\MessengerDataCollector as BaseMessengerDataCollector;

/**
 * Messenger data collector.
 */
class MessengerDataCollector extends BaseMessengerDataCollector implements HasPanelInterface {

  use StringTranslationTrait;
  use DumpTrait;

  /**
   * {@inheritdoc}
   */
  public function getPanel(): array {
    if (count($this->getMessages()) == 0) {
      return [
        '#markup' => $this->t('No messages have been collected'),
      ];
    }

    $rows = [];
    foreach ($this->getMessages() as $message) {
      $rows[] = [
        [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{{ data|raw }}',
            '#context' => [
              'data' => $this->dumpData($message),
            ],
          ],
          'class' => 'webprofiler__value',
        ],
      ];
    }

    return [
      '#theme' => 'webprofiler_dashboard_section',
      '#title' => 'Messages',
      '#data' => [
        '#type' => 'table',
        '#rows' => $rows,
        '#attributes' => [
          'class' => [
            'webprofiler__table',
          ],
        ],
      ],
    ];
  }

}
