<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger;

/**
 * Interface for handler responses.
 */
interface HandlerResponseInterface {

  /**
   * Returns the response.
   *
   * @return string
   *   The response.
   */
  public function getResponse(): string;

}
