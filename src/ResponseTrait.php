<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\HandledStamp;

/**
 * Response trait.
 */
trait ResponseTrait {

  /**
   * Get handler result.
   */
  public function getHandlerResult(Envelope $envelope): mixed {
    return $envelope
      ->last(HandledStamp::class)
      ?->getResult();
  }

}
