<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\symfony_messenger\DataCollector\MessengerDataCollector;
use Symfony\Component\Messenger\DependencyInjection\MessengerPass;

/**
 * Registers the Symfony Messenger service provider.
 */
class SymfonyMessengerServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container
      // Priority 200 to execute before AttributeAutoconfigurationPass which has
      // priority 100. See symfony/dependency-injection/Compiler/PassConfig.php.
      ->addCompilerPass(new SymfonyMessengerMessageHandlerCompilerPass(), priority: 200)
      ->addCompilerPass(new MessengerPass());

    $modules = $container->getParameter('container.modules');

    // Add BlockDataCollector only if Block module is enabled.
    if (isset($modules['webprofiler'])) {
      $container->register('data_collector.messenger',
        MessengerDataCollector::class)
        ->addTag('data_collector', [
          'template' => '@symfony_messenger/Collector/messenger.html.twig',
          'id' => 'messenger',
          'label' => 'Messenger',
          'priority' => 500,
        ]);
    }
  }

}
