<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test\Messenger\Subdir;

use Drupal\symfony_messenger_test\TestSubdirMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

/**
 * Message handler in a subdirectory.
 */
#[AsMessageHandler]
final class TestMessageSubdirHandler {

  /**
   * Message handler for TestSubdirMessage messages.
   */
  public function __invoke(TestSubdirMessage $message): void {
    $message->handledBy = __METHOD__;
  }

}
