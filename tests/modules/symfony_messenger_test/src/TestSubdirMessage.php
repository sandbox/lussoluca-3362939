<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test;

/**
 * @see \Drupal\symfony_messenger_test\Messenger\Subdir\TestMessageSubdirHandler
 */
final class TestSubdirMessage {

  /**
   * Creates a new TestSubdirMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
