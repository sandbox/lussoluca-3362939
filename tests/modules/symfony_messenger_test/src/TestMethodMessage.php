<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test;

/**
 * @see \Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler
 */
final class TestMethodMessage {

  /**
   * Creates a new TestMethodMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
