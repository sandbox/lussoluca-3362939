<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test;

/**
 * Message handler outside of Messenger/ dir and does not use attribute.
 */
final class TestMessageServiceHandler {

  /**
   * Message handler for TestServiceMessage messages.
   */
  public function __invoke(TestServiceMessage $message): void {
    $message->handledBy = __METHOD__;
  }

}
