<?php

declare(strict_types = 1);

namespace Drupal\Tests\symfony_messenger\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\symfony_messenger_test\TestClassMessage;
use Drupal\symfony_messenger_test\TestMethod2Message;
use Drupal\symfony_messenger_test\TestMethodMessage;
use Drupal\symfony_messenger_test\TestServiceMessage;
use Drupal\symfony_messenger_test\TestSubdirMessage;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Tests Symfony Messenger.
 */
class SymfonyMessengerMessageHandlerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'symfony_messenger_test',
    'symfony_messenger',
  ];

  /**
   * Tests #[AsMessageHandler] on class.
   *
   * @see \Drupal\symfony_messenger_test\Messenger\TestMessageClassHandler
   */
  public function testAttributeOnClass(): void {
    $message = new TestClassMessage();
    $this->bus()->dispatch($message);
    $this->assertEquals('Drupal\symfony_messenger_test\Messenger\TestMessageClassHandler::__invoke', $message->handledBy);
  }

  /**
   * Tests #[AsMessageHandler] on class in a subdirectory of Messenger/.
   *
   * @see \Drupal\symfony_messenger_test\Messenger\Subdir\TestMessageSubdirHandler
   */
  public function testAttributeOnClassInSubdir(): void {
    $message = new TestSubdirMessage();
    $this->bus()->dispatch($message);
    $this->assertEquals('Drupal\symfony_messenger_test\Messenger\Subdir\TestMessageSubdirHandler::__invoke', $message->handledBy);
  }

  /**
   * Tests #[AsMessageHandler] on methods.
   *
   * @see \Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler::fooBar()
   * @see \Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler::fooBar2()
   */
  public function testAttributeOnMethod(): void {
    $message = new TestMethodMessage();
    $this->bus()->dispatch($message);
    $this->assertEquals('Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler::fooBar', $message->handledBy);

    $message = new TestMethod2Message();
    $this->bus()->dispatch($message);
    $this->assertEquals('Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler::fooBar2', $message->handledBy);
  }

  /**
   * Tests message handler defined as a service.
   *
   * Without attribute and outside of Messenger/ dir.
   *
   * @see \Drupal\symfony_messenger_test\TestMessageServiceHandler
   */
  public function testService(): void {
    $message = new TestServiceMessage();
    $this->bus()->dispatch($message);
    $this->assertEquals('Drupal\symfony_messenger_test\TestMessageServiceHandler::__invoke', $message->handledBy);
  }

  /**
   * Default messenger bus.
   */
  private function bus(): MessageBusInterface {
    return \Drupal::service('symfony_messenger.bus.default');
  }

}
